package main

import (
	"encoding/json"
	"goblog/db"
	"log"
	"math/rand"
	"net/http"
	"strconv"

	_ "github.com/golang-migrate/migrate/source/file"
	"github.com/gorilla/mux"
	_ "github.com/lib/pq"
)


type Post struct {
	Id     string  `json:"id"`
	Title  string  `json:"title"`
	Body   string  `json:"body"`
	Author *Author `json:"author"`
}

type Author struct {
	FirstName string `json:"firstname"`
	LastName  string `json:"lastname"`
}

var posts []Post

func createPost(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var post Post
	jsonEncoder := json.NewEncoder(w)

	err := json.NewDecoder(r.Body).Decode(&post)
	if err != nil {
		jsonEncoder.Encode(err)
		return
	}

	post.Id = strconv.Itoa(rand.Intn(15101991))
	posts = append(posts, post)
	jsonEncoder.Encode(post)
}

func getPost(w http.ResponseWriter, r *http.Request) { //error {
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r)
	for _, item := range posts {
		if item.Id == params["id"] {
			json.NewEncoder(w).Encode(item)
			return
		}
	}
	json.NewEncoder(w).Encode(&Post{})
}

func getPosts(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(posts)
}

func updatePost(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r)
	for i, item := range posts {
		if item.Id == params["id"] {
			posts = append(posts[:i], posts[i+1:]...)
			var post Post
			_ = json.NewDecoder(r.Body).Decode(&post)
			post.Id = params["id"]
			posts = append(posts, post)
			json.NewEncoder(w).Encode(post)
			return
		}
	}
}

func deletePost(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r)
	for index, item := range posts {
		if item.Id == params["id"] {
			posts = append(posts[:index], posts[index+1:]...)
			break
		}
	}
	json.NewEncoder(w).Encode(posts)
}

func main() {
	database := db.NewDB()
	database.Connect()
	database.Migrate()

	// Init Router
	router := mux.NewRouter()

	// Mock Data - @todo implement DB
	posts = append(posts, Post{Id: "1", Title: "My Title", Body: "My Body", Author: &Author{FirstName: "Daniel", LastName: "Shterenberg"}})
	posts = append(posts, Post{Id: "2", Title: "My Title 2", Body: "My Body 2", Author: &Author{FirstName: "Daniel", LastName: "Shterenberg"}})
	posts = append(posts, Post{Id: "3", Title: "My Title 3", Body: "My Body 3", Author: &Author{FirstName: "Daniel", LastName: "Shterenberg"}})
	posts = append(posts, Post{Id: "4", Title: "My Title 4", Body: "My Body 4", Author: &Author{FirstName: "Daniel", LastName: "Shterenberg"}})

	// Route Handlers / Endpoints
	router.HandleFunc("/api/posts", getPosts).Methods("GET")
	router.HandleFunc("/api/posts/{id}", getPost).Methods("GET")
	router.HandleFunc("/api/posts", createPost).Methods("POST")
	router.HandleFunc("/api/posts/{id}", updatePost).Methods("PUT")
	router.HandleFunc("/api/posts/{id}", deletePost).Methods("DELETE")

	log.Fatal(http.ListenAndServe(":8080", router))
}

