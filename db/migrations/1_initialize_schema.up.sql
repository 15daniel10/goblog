CREATE TABLE IF NOT EXISTS users (
  user_id integer unique,
  name    varchar(40),
  email   varchar(40)
);