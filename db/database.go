package db

import (
	"database/sql"
	"fmt"
	log "github.com/Sirupsen/logrus"
	"github.com/golang-migrate/migrate"
	"github.com/golang-migrate/migrate/database/postgres"
	_ "github.com/golang-migrate/migrate/source/github"
	"strconv"
)

const (
	host     = "localhost"
	port     = 5432
	user     = "postgres"
	password = "postgres"
	dbname   = "goblog"
)

type dbInteface interface {
	Connect() error
	Migrate() error
}

type DB struct {
 	conn *sql.DB
}

func NewDB() dbInteface{
	return &DB{}
}

func (db *DB) Connect() error{
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbname)

	conn, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		panic(err)
	}
	//defer conn.Close()
	log.Info("Connection SUCCESS")
	db.conn = conn
	return nil
}

func (db *DB) Migrate() error{
	log.Info("Migrating ", db.conn)
	driver, err := postgres.WithInstance(db.conn, &postgres.Config{})
	if err != nil {
		log.Fatal(err)
	}
	log.Info("Got DB driver.")
	m, err := migrate.NewWithDatabaseInstance("file://db/migrations/", "postgres", driver)
	if err != nil {
		return err
	}
	log.Info("Opened Migration Store.")
	version, dirty, err := m.Version()
	if err != nil {
		return err
	}

	log.Info("Migrating: Current schema version ", strconv.FormatInt(int64(version), 10), " dirty: ", strconv.FormatBool(dirty))
	err = m.Up()
	if err != nil {
		return err
	}
	log.Info("Migration SUCCESS")
	return nil
}
